package com.example.movieapp.models;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;

import androidx.annotation.RequiresApi;
import androidx.databinding.BindingAdapter;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "movie_table",indices = @Index(value = {"imdbID"}, unique = true))
public class Movie implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @SerializedName("imdbID")
    private String imdbID;
    @SerializedName("Title")
    private String title;
    @SerializedName("Year")
    private String year;
    @SerializedName("Type")
    private String type;
    @SerializedName("Poster")
    private String poster;
    @SerializedName("Released")
    private String released;
    @SerializedName("Runtime")
    private String runtime;
    @SerializedName("Plot")
    private String plot;
    @SerializedName("imdbRating")
    private String imdbRating;
    @SerializedName("Genre")
    private String genres;
    private boolean favorite = false;

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getImdbRating() {
        return imdbRating +"/10";
    }

    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @BindingAdapter({ "image" })
    public static void loadImage(ImageView imageView, String imgUrl) {
        Picasso.get().load(imgUrl).into(imageView);
    }

    public Movie(){

    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    protected Movie(Parcel in){
        id = in.readInt();
        imdbID = in.readString();
        title = in.readString();
        year = in.readString();
        type = in.readString();
        poster = in.readString();
        released = in.readString();
        runtime = in.readString();
        plot = in.readString();
        imdbRating = in.readString();
        genres = in.readString();
        favorite = in.readInt() == 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(imdbID);
        parcel.writeString(title);
        parcel.writeString(year);
        parcel.writeString(type);
        parcel.writeString(poster);
        parcel.writeString(released);
        parcel.writeString(runtime);
        parcel.writeString(plot);
        parcel.writeString(imdbRating);
        parcel.writeString(genres);
        parcel.writeInt(favorite ? 1:0);
    }
}

