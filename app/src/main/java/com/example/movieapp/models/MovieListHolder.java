package com.example.movieapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieListHolder {
    @SerializedName("Search")
    private List<Movie> movies;

    public List<Movie> getMovies() {
        return movies;
    }
}
