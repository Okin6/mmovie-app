package com.example.movieapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.movieapp.R;
import com.example.movieapp.fragments.FavoriteMoviesFragment;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Provjera postoji li container fragmenata

        if(findViewById(R.id.fragment_container) != null){
            if(savedInstanceState != null){
                return;
            }
            FavoriteMoviesFragment favoriteMoviesFragment = new FavoriteMoviesFragment();
            favoriteMoviesFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, favoriteMoviesFragment).commit();
        }



    }
}