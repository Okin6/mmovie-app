package com.example.movieapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.movieapp.R;
import com.example.movieapp.databinding.MovieItemBinding;
import com.example.movieapp.models.Movie;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieHolder> {

    private List<Movie> movieList = new ArrayList<>();
    private OnMovieListener onMovieListener;
    private OnFavoriteListener onFavoriteListener;
    private List<MovieHolder> holders = new ArrayList<>();

    public MoviesAdapter(OnMovieListener onMovieListener, OnFavoriteListener onFavoriteListener){
        this.onMovieListener = onMovieListener;
        this.onFavoriteListener = onFavoriteListener;
    }

    //Pronalazak specificnog holder-a, koji sadrzi film ciju je favorite ikonu potrebno azurirati
    public void findHolder(Movie movie){
        for(MovieHolder holder : holders){
            if(holder.movieItemBinding.getMovie().getImdbID().equals(movie.getImdbID())){
                holder.changeButtonBackground(movie.isFavorite());
            }
        }
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MovieItemBinding movieItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.movie_item,parent,false);
        MovieHolder holder = new MovieHolder(movieItemBinding,onMovieListener,onFavoriteListener);
        holders.add(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        Movie currentMovie = movieList.get(position);
        holder.movieItemBinding.setMovie(currentMovie);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public void setMovieList(List<Movie> movies){
        this.movieList = movies;
        notifyDataSetChanged();
    }

    public class MovieHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private MovieItemBinding movieItemBinding;
        private OnMovieListener onMovieListener;
        private OnFavoriteListener onFavoriteListener;

        public MovieHolder(@NonNull MovieItemBinding movieItemBinding, OnMovieListener onMovieListener, OnFavoriteListener onFavoriteListener) {
            super(movieItemBinding.getRoot());
            this.movieItemBinding = movieItemBinding;
            this.onMovieListener = onMovieListener;
            this.onFavoriteListener = onFavoriteListener;
            movieItemBinding.getRoot().setOnClickListener(this);

            //Obrada favorite klika
            movieItemBinding.favoriteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Movie m = movieItemBinding.getMovie();
                    if(!m.isFavorite()){
                        movieItemBinding.favoriteButton.setBackgroundResource(R.drawable.ic_baseline_favorite_24);
                    } else {
                        movieItemBinding.favoriteButton.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24);
                    }
                    onFavoriteListener.onFavoriteClick(getAdapterPosition());
                }
            });
        }
        //Metoda za azuriranje ikone
        public void changeButtonBackground(boolean favorite){
            if(favorite){
                movieItemBinding.favoriteButton.setBackgroundResource(R.drawable.ic_baseline_favorite_24);
            }else{
                movieItemBinding.favoriteButton.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24);
            }
        }

        @Override
        public void onClick(View view) {
            onMovieListener.onMovieClick(getAdapterPosition());
        }
    }

    //Sucelje za klik na film
    public interface OnMovieListener{
        void onMovieClick(int position);
    }

    //Sucelje za klik na favorite
    public interface OnFavoriteListener{
        void onFavoriteClick(int position);
    }
}
