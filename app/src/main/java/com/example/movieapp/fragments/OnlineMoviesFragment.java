package com.example.movieapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.movieapp.R;
import com.example.movieapp.adapters.MoviesAdapter;
import com.example.movieapp.models.MovieListHolder;
import com.example.movieapp.models.Movie;
import com.example.movieapp.utils.FragmentCallback;
import com.example.movieapp.viewmodels.HomeViewModel;
import com.example.movieapp.databinding.FragmentMoviesListBinding;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class OnlineMoviesFragment extends Fragment implements MoviesAdapter.OnMovieListener, MoviesAdapter.OnFavoriteListener, FragmentCallback {

    private HomeViewModel homeViewModel;
    private List<Movie> moviesList;
    private RecyclerView moviesView;
    private MoviesAdapter moviesAdapter;
    private Movie movieForDetails = new Movie();
    private int position;
    private EditText editText;
    private RelativeLayout loading;
    private ConstraintLayout noResults;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentMoviesListBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_movies_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        moviesView = getView().findViewById(R.id.moviesView);
        editText = getView().findViewById(R.id.searchBar);
        loading = getView().findViewById(R.id.loadingPanel);
        noResults = getView().findViewById(R.id.noResult);

        loading.setVisibility(View.VISIBLE);

        moviesView.setLayoutManager(new LinearLayoutManager(getActivity()));
        moviesView.setHasFixedSize(true);
        moviesAdapter = new MoviesAdapter(this,this);
        moviesView.setAdapter(moviesAdapter);

        homeViewModel = new ViewModelProvider(requireActivity()).get(HomeViewModel.class);

        homeViewModel.observeMovies().observe(getViewLifecycleOwner(), new Observer<MovieListHolder>() {
            @Override
            public void onChanged(MovieListHolder movieListHolder) {
                loading.setVisibility(View.GONE);
                if(movieListHolder.getMovies() != null){
                    noResults.setVisibility(View.GONE);
                    moviesList = movieListHolder.getMovies();
                    moviesAdapter.setMovieList(moviesList);
                }else{
                    noResults.setVisibility(View.VISIBLE);
                }
            }
        });

        homeViewModel.observeMovieForDetails().observe(getViewLifecycleOwner(), new Observer<Movie>() {
            @Override
            public void onChanged(Movie movie) {
                if(movie != null){
                    //Obogaćivanje i prosljeđivanje filma za fragment sa detaljima o filmu
                    movie.setFavorite(movieForDetails.isFavorite());
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("Movie",movie);
                    bundle.putInt("Position",position);
                    MovieDetailsFragment movieDetailsFragment = new MovieDetailsFragment();
                    movieDetailsFragment.setArguments(bundle);
                    movieDetailsFragment.setFragmentCallback(OnlineMoviesFragment.this);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.hide(OnlineMoviesFragment.this);
                    transaction.add(R.id.fragment_container, movieDetailsFragment, "movieDetailsFragment");
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        });

            //Pokretanje pretrage filmova po nazivu
            Bundle bundlePassed = getArguments();
            searchMovies(bundlePassed.getString("title"));


        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent event) {
                //Obrada entera i teksta u edittextu
                if ((event != null && (event.getKeyCode() == android.view.KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE)) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    String movieTitle = editText.getText().toString();
                    movieTitle.trim();
                    movieTitle = movieTitle.replace(" ", "+");
                    searchMovies(movieTitle);
                }
                return false;
            }
        });
    }

    private void searchMovies(String title){
        homeViewModel.getMoviesByTitle(title);
    }

    @Override
    public void onMovieClick(int position) {
        movieForDetails = moviesList.get(position);
        this.position = position;
        homeViewModel.getMoviesById(movieForDetails.getImdbID());
    }

    //Obrada favorite klika, gdje se dodaje ili mice iz baze
    @Override
    public void onFavoriteClick(int position) {
        Movie movieFavoriteClicked = moviesList.get(position);
        if(!movieFavoriteClicked.isFavorite()){
            movieFavoriteClicked.setFavorite(true);
            homeViewModel.insertMovie(movieFavoriteClicked);
        } else {
            movieFavoriteClicked.setFavorite(false);
            homeViewModel.deleteMovie(movieFavoriteClicked.getImdbID());
            }
    }

    //Metoda za azuriranje liste, poziva se ako korisnik u fragmentu detalja filma ga doda ili makne sa favorites-a
    @Override
    public void updateMovie(Movie movieToUpdate, int position) {
        if(!moviesList.get(position).isFavorite() == movieToUpdate.isFavorite()){
            moviesList.get(position).setFavorite(movieToUpdate.isFavorite());
            moviesAdapter.findHolder(movieToUpdate);
        }
    }

}
