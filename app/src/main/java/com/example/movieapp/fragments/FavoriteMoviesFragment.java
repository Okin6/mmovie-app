package com.example.movieapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.movieapp.R;
import com.example.movieapp.adapters.MoviesAdapter;
import com.example.movieapp.models.Movie;
import com.example.movieapp.viewmodels.HomeViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FavoriteMoviesFragment extends Fragment implements MoviesAdapter.OnMovieListener, MoviesAdapter.OnFavoriteListener {

    private HomeViewModel homeViewModel;
    private List<Movie> moviesList;
    private RecyclerView moviesView;
    private MoviesAdapter moviesAdapter;
    private EditText editText;
    private RelativeLayout loading;
    private ConstraintLayout emptyListTag;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movies_list, container,false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        moviesView = getView().findViewById(R.id.moviesView);
        editText = getView().findViewById(R.id.searchBar);
        loading = getView().findViewById(R.id.loadingPanel);
        emptyListTag = getView().findViewById(R.id.emptyList);

        moviesView.setLayoutManager(new LinearLayoutManager(getActivity()));
        moviesView.setHasFixedSize(true);
        moviesAdapter = new MoviesAdapter(this, this);
        moviesView.setAdapter(moviesAdapter);

        homeViewModel = new ViewModelProvider(requireActivity()).get(HomeViewModel.class);

        homeViewModel.getDatabaseMovies().observe(getViewLifecycleOwner(), new Observer<List<Movie>>() {
            @Override
            public void onChanged(List<Movie> movies) {
                if(movies != null){
                    if(movies.isEmpty()){
                        emptyListTag.setVisibility(View.VISIBLE);
                    }
                    else {
                        emptyListTag.setVisibility(View.GONE);
                    }
                    moviesList = movies;
                    moviesAdapter.setMovieList(moviesList);
                    loading.setVisibility(View.GONE);
                }
            }
        });

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent event) {
                //Obrada entera i teksta u edittextu
                if ((event != null && (event.getKeyCode() == android.view.KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE)) {
                    loading.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    String movieTitle = editText.getText().toString();
                    movieTitle.trim();
                    movieTitle = movieTitle.replace(" ", "+");

                    //Podaci za iduci fragment
                    Bundle bundle = new Bundle();
                    bundle.putString("title",movieTitle);
                    OnlineMoviesFragment onlineMoviesFragment = new OnlineMoviesFragment();
                    onlineMoviesFragment.setArguments(bundle);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, onlineMoviesFragment, "onlineMoviesFragment");
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
                return false;
            }
        });
    }


    @Override
    public void onMovieClick(int position) {

    }

    @Override
    public void onFavoriteClick(int position) {
        Movie movieToDelete = moviesList.get(position);
        homeViewModel.deleteMovie(movieToDelete.getImdbID());
    }
}
