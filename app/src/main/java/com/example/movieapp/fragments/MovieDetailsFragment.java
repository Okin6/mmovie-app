package com.example.movieapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.movieapp.R;
import com.example.movieapp.databinding.FragmentMovieDetailsBinding;
import com.example.movieapp.models.Movie;
import com.example.movieapp.utils.FragmentCallback;
import com.example.movieapp.viewmodels.HomeViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class MovieDetailsFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentCallback fragmentCallback;
    private FragmentMovieDetailsBinding binding;

    public void setFragmentCallback(FragmentCallback fragmentCallback) {
        this.fragmentCallback = fragmentCallback;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_movie_details, container, false);
        View view = binding.getRoot();

        //Dohvacanje prosljedenih podataka
        Bundle bundle = getArguments();
        Movie movie = bundle.getParcelable("Movie");
        int position = bundle.getInt("Position");
        binding.setMovie(movie);

        homeViewModel = new ViewModelProvider(requireActivity()).get(HomeViewModel.class);

        //Obrada favorite klika i prosljedivanje podataka kako bi se azurirali na prethodnom fragmentu
        binding.favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!movie.isFavorite()){
                    binding.favoriteButton.setBackgroundResource(R.drawable.ic_baseline_favorite_24_white);
                    movie.setFavorite(true);
                    homeViewModel.insertMovie(movie);
                    fragmentCallback.updateMovie(movie,position);
                } else {
                    binding.favoriteButton.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24_white);
                    movie.setFavorite(false);
                    homeViewModel.deleteMovie(movie.getImdbID());
                    fragmentCallback.updateMovie(movie,position);
                }
            }
        });


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Setup toolbar-a i back klika
        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

    }

    //Omogucivanje back klika u toolbar-u
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
