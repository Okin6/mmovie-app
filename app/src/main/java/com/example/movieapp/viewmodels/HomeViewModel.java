package com.example.movieapp.viewmodels;

import android.app.Application;
import android.os.Build;

import com.example.movieapp.models.MovieListHolder;
import com.example.movieapp.models.Movie;
import com.example.movieapp.repositories.LocalMovieRepository;
import com.example.movieapp.repositories.OnlineMovieRepository;
import com.example.movieapp.utils.SingleLiveEvent;

import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class HomeViewModel extends AndroidViewModel {

    private OnlineMovieRepository onlineMovieRepository;
    private LocalMovieRepository localMovieRepository;
    private LiveData<List<Movie>> databaseMovies;
    private Single<MovieListHolder> observableMovies;
    private Single<Movie> observableMovie;
    private SingleLiveEvent<MovieListHolder> gottenMovies = new SingleLiveEvent<>();
    private SingleLiveEvent<Movie> movieForDetails = new SingleLiveEvent<>();

    public HomeViewModel(@NonNull Application application) {
        super(application);
        onlineMovieRepository = new OnlineMovieRepository();
        localMovieRepository = new LocalMovieRepository(application);
        databaseMovies = localMovieRepository.getLocalMovies();
    }


    public LiveData<List<Movie>> getDatabaseMovies(){
        return databaseMovies;
    }

    public void getMoviesByTitle(String movieTitle){
        observableMovies = onlineMovieRepository.getMoviesByTitle(movieTitle);
        observableMovies.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<MovieListHolder>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                    }

                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull MovieListHolder movies) {
                        //Postavljanje favorite dohvacenim filmovima ako ih vec ima u lokalnoj bazi
                        if(databaseMovies.getValue() != null && movies.getMovies() != null) {
                                for (Movie m : databaseMovies.getValue()) {
                                    for (Movie movie : movies.getMovies()) {
                                        if (m.getImdbID().equals(movie.getImdbID())) {
                                            if (m.isFavorite()) {
                                                movie.setFavorite(true);
                                            }
                                        }
                                    }
                                }
                            movies.getMovies().sort(Comparator.comparing(Movie::getYear));
                            }
                            gottenMovies.setValue(movies);
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                    }
                });
    }

    public void getMoviesById(String movieId){
        observableMovie = onlineMovieRepository.getMoviesById(movieId);
        observableMovie.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Movie>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Movie movie) {
                        movieForDetails.setValue(movie);
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                    }
                });
    }

    public SingleLiveEvent<MovieListHolder> observeMovies(){
        return gottenMovies;
    }

    public SingleLiveEvent<Movie> observeMovieForDetails(){
        return movieForDetails;
    }

    public void insertMovie(Movie movie){
        localMovieRepository.insert(movie);
    }

    public void deleteMovie(String imdbID){
        localMovieRepository.delete(imdbID);
    }
}
