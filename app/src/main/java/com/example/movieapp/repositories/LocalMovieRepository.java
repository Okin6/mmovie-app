package com.example.movieapp.repositories;

import android.app.Application;
import android.os.AsyncTask;

import com.example.movieapp.models.Movie;
import com.example.movieapp.room.MovieDao;
import com.example.movieapp.room.MovieDatabase;

import java.util.List;

import androidx.lifecycle.LiveData;

public class LocalMovieRepository {

    private MovieDao movieDao;
    private LiveData<List<Movie>> localMovies;

    public LocalMovieRepository(Application application){
        MovieDatabase database = MovieDatabase.getInstance(application);
        movieDao = database.movieDao();
        localMovies = movieDao.getAllMovies();
    }

    public LiveData<List<Movie>> getLocalMovies() {
        return localMovies;
    }

    //Insert metode
    public void insert(Movie movie){
        new InsertMovieAsyncTask(movieDao).execute(movie);
    }

    private static class InsertMovieAsyncTask extends AsyncTask<Movie,Void,Void> {
        private MovieDao movieDao;

        private InsertMovieAsyncTask(MovieDao movieDao){
            this.movieDao = movieDao;
        }

        @Override
        protected Void doInBackground(Movie... movies) {
            movieDao.insert(movies[0]);
            return null;
        }
    }

    //Delete metode
    public void delete(String imdbID){
        new DeleteMovieAsyncTask(movieDao).execute(imdbID);
    }

    private static class DeleteMovieAsyncTask extends AsyncTask<String,Void,Void> {
        private MovieDao movieDao;

        private DeleteMovieAsyncTask(MovieDao movieDao){
            this.movieDao = movieDao;
        }

        @Override
        protected Void doInBackground(String... movies) {
            movieDao.delete(movies[0]);
            return null;
        }
    }
}
