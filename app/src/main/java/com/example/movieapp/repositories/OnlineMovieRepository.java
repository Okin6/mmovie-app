package com.example.movieapp.repositories;

import com.example.movieapp.models.MovieListHolder;
import com.example.movieapp.models.Movie;

import java.util.HashMap;
import java.util.Map;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import io.reactivex.rxjava3.core.Single;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OnlineMovieRepository {

    private SearchAPI searchAPI;
    private Single<MovieListHolder> movies;
    private Single<Movie> movie;
    private Map<String,String> searchData = new HashMap<>();

    public OnlineMovieRepository(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.omdbapi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build();
        searchAPI = retrofit.create(SearchAPI.class);
        searchData.put("apikey","8797af0f");
    }

    public Single<MovieListHolder> getMoviesByTitle(String movieTitle){
        searchData.put("s",movieTitle);
        movies = searchAPI.getMoviesByTitle(searchData);
        return movies;
    }
    public Single<Movie> getMoviesById(String id){
        searchData.remove("s");
        searchData.put("i",id);
        movie = searchAPI.getMoviesById(searchData);
        return movie;
    }
}
