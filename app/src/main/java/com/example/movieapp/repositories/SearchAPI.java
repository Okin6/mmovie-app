package com.example.movieapp.repositories;

import com.example.movieapp.models.MovieListHolder;
import com.example.movieapp.models.Movie;

import java.util.Map;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface SearchAPI {
    @GET(".")
    Single<MovieListHolder> getMoviesByTitle(@QueryMap Map<String, String> searchData);
    @GET(".")
    Single<Movie> getMoviesById(@QueryMap Map<String, String> searchData);
}
