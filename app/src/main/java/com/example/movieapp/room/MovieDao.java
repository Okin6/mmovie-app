package com.example.movieapp.room;

import com.example.movieapp.models.Movie;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface MovieDao {
    @Query("SELECT * FROM movie_table ORDER BY year ASC")
    LiveData<List<Movie>> getAllMovies();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Movie movie);

    @Query("DELETE FROM movie_table WHERE imdbID = :imdbID")
    void delete(String imdbID);
}
