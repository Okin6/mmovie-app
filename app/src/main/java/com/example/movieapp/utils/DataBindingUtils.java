package com.example.movieapp.utils;

import android.widget.ImageView;

import com.example.movieapp.R;

import androidx.databinding.BindingAdapter;

public class DataBindingUtils {

    //Binding adapteri za prikaz favorite ikona

    @BindingAdapter("isFavorite")
    public static void setImageUri(ImageView button,Boolean isFavorite) {
        if (isFavorite) {
            button.setBackgroundResource(R.drawable.ic_baseline_favorite_24);
        } else {
            button.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24);
        }
    }
    @BindingAdapter("isFavorite_white")
    public static void setImageUriWhite(ImageView button,Boolean isFavorite) {
        if (isFavorite) {
            button.setBackgroundResource(R.drawable.ic_baseline_favorite_24_white);
        } else {
            button.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24_white);
        }
    }
}
