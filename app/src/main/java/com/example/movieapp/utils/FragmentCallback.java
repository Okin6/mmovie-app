package com.example.movieapp.utils;

import com.example.movieapp.models.Movie;

//Sucelje za azuriranje prethodnog fragmenta

public interface FragmentCallback {
    void updateMovie(Movie movie,int position);
}
